(function( $ ) {
 
    $.fn.carousel = function(options) {

        var defaults = {   
            debug:      false,
            images:     6,
            height:     100,
            speed:      1000,
            spacing:     0,
            prev:       '.m-carousel-prev',
            next:       '.m-carousel-next',
            container:  '.m-carousel-container',
            overflow:   '.m-carousel-overflow',
            image:      '.m-carousel-image'
        };
     
        var settings = $.extend( {}, defaults, options );

        var getSpeed = function(obj) {
            var speed = settings.speed;
            if(obj.data('speed')) {
                speed = obj.data('speed');
            }
            return speed;
        }

        var getImagesVisible = function(obj) {
            var images = settings.images;
            if(obj.data('images')) {
                images = obj.data('images');
            }
            return images;
        }

        var getCarouselHeight = function(obj) {
            var height = settings.height;
            if(obj.data('images')) {
                height = obj.data('height');
            }
            return height;
        }

        var getSpacing = function(obj) {
            var spacing = settings.spacing;
            if(obj.data('spacing')) {
                spacing = obj.data('spacing');
            }
            return spacing;
        }

        return this.each(function() {
            var self = $(this);

            //determine transition speed
            var speed = getSpeed(self);

            //determine how many images should be visible
            var imagesVisible = getImagesVisible(self);

            //get carousel height
            var carouselHeight = getCarouselHeight(self);

            //set image thumb width so everything looks balanced
            var spacing = getSpacing(self);

            var prevButton = settings.prev;
            if($(this).data('prev')) {
                prevButton = $(this).data('prev');
            }

            var nextButton = settings.next;
            if($(this).data('next')) {
                nextButton = $(this).data('next');
            }

            if(self.find(settings.image).length <= imagesVisible) {
                self.find(prevButton).hide();
                self.find(nextButton).hide();
            }

            var showPrevious = function() {
                var currentLeft = parseFloat( self.find(settings.overflow).css('margin-left'), 16);

                self.find(settings.overflow).css({
                    'margin-left': currentLeft - imageWidth  
                });

                self.find(settings.image + ':last').insertBefore( self.find(settings.image + ':first') );

                //animate slide
                self.find(settings.overflow).animate({
                    'margin-left': 0
                }, speed, function() {
                    //execute changed callback
                    if(self.data('changed')) {
                        window[self.data('changed')](self.find(settings.image + ':first'));
                    }
                });
            }

            var showNext = function() {
                var currentLeft = parseFloat( self.find(settings.overflow).css('margin-left'), 16);
                var newLeft = currentLeft - imageWidth;

                //animate slide
                self.find(settings.overflow).animate({
                    'margin-left': newLeft
                }, speed, function() {

                    self.find(settings.overflow).css({
                        'margin-left': 0  
                    });

                    self.find(settings.image + ':first').insertAfter( self.find(settings.image + ':last') );

                    //execute changed callback
                    if(self.data('changed')) {
                        window[self.data('changed')](self.find(settings.image + ':first'));
                    }
                });
            }


            /* INITIALISE -------------------------------- */

            //run load callback
            if(self.data('load')) {
                window[self.data('load')]();
            }

            //if scroll is set, automaticalling scroll through carousel
            if(self.data('scroll')) {
                setInterval(function(){
                    showNext();
                }, self.data('scroll'));
            }
        
            var imageWidth = ( ($(settings.container).width() / imagesVisible));
            self.find(settings.image).css({
                'width':        imageWidth - spacing,
                'height':       carouselHeight
            });

            //set width of overflow wrapper
            var overflowWidth = (self.find(settings.image).length * imageWidth);
            self.find(settings.overflow).css({
                'width':        overflowWidth,
                'margin-left':  '0'
            });

            //on load, set images to remove img tags and set images as background of divs to be responsive
            self.find(settings.image).each(function(i, val) {
                $(this).css({
                    'background-image': 'url("' + $(this).data('src')  + '")'
                }).attr('data-index', i + 1);
            });

            //run loaded callback
            if(self.data('loaded')) {
                window[self.data('loaded')]();
            }


            /* EVENTS -------------------------------- */

            self.find(prevButton).off().on('click', function(e) {
                e.preventDefault();
                showPrevious();
            });

            self.find(nextButton).off().on('click', function(e) {
                e.preventDefault();
                showNext();
            });

            if(self.data('selected')) {
                self.find( settings.image ).off().on('click', function(e) {
                    window[self.data('selected')]( $(this) );
                });
            }

        });
    };

}( jQuery ));