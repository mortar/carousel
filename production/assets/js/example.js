/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/carousel
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-06-04
---------------------------------------------------------------------------- */
$('.m-carousel').carousel({
	debug: 		true
});

function load() {
	console.log('load');
}

function loaded() {
	console.log('loaded');
}

function changed(obj) {
	console.log('changed', obj);
}

function selected(obj) {
	console.log('selected', obj);
}